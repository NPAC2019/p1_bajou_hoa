#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#implement the ILC method to extract the CMB from the original Planck maps
"""
Created on Tue Mar 12 11:11:43 2019

@author: bajou
"""

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import inv
import numpy.ma as ma
#import maps as mp   #load the script that allows to read smoothed Planck maps  

plt.ion()

main_path = "/home/bajou/p1_bajou_hoa/ComputerProject/plots/"


freq = np.array([100,143,217,353,545,857])   #the frequencies of the Planck skymaps (GHz)
freq_dic = {100: 100, 143: 143, 217: 217, 353: 353, 545: 545, 857: 857}

NSIDE= 2048   #pixel resolution of the skymaps


maps = []   #list that contains the smoothed (and eventually degraded) Planck skymaps. 
half_1 = []
half_2 = []


for i in range(len(freq)):
    maps.append(hp.read_map(main_path+"smooth/smooth_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))
    half_1.append(hp.read_map(main_path+"half_mission_1/half_1_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))
    half_2.append(hp.read_map(main_path+"half_mission_2/half_2_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))


def Inv_cov_mat(m):
    '''
    Compute the inverse covariance matrix from the 'm' array containing the N_obs=6 Planck maps
    '''
    cov=np.zeros((len(freq),len(freq)))
    
    for i in range(len(freq)):
        meani  = np.mean(m[i])      
        npix   = np.size(m[i])      
        for j in range(len(freq)):
            meanj  = np.mean(m[j])
            cov[i][j] = (m[i]-meani).dot(m[j]-meanj) / npix
    
    return inv(cov)


def Weights(f_c, icov):
    '''
    Inputs: - 'f_c' (array like): constraints for the astrophysical component of interest 
            - 'i_cov' (array like): inverse of the covariance matrix
    Output: - the weights obtained with the single-component ILC method
    '''
    return f_c.T.dot(icov)/(f_c.T.dot(icov).dot(f_c))


def Weights_const(e, F, icov):
    '''
    Inputs: - 'e' (array like) of size equal to N_c + 1, which is the number of astrophysical components taken into account
            - 'F' (array like) matrix of size N_c+1 x N_obs
    Compute the weights of the maps for the constrained ILC
    '''
    return e.T.dot(inv(F.T.dot(icov).dot(F))).dot(F.T).dot(icov)



def Final_map(w,m):
    '''
    Inputs: 'w' (array like) whose elements are the weights of each Planck map, 'm' (array like) whose elements are the skymaps;
    Output: the final linear combination of the weighted maps (array like)
    '''
    return w.dot(m)

#hp.write_map(main_path+"CMB_map_simple_ILC", Final_map(simple_w, maps))

#hp.write_map(main_path+"CMB_map_constrained_ILC", Final_map(const_w, maps))

def GetMask(r): 
    '''
    Input: 'r' (float type) which will define the portion of the masked sky, e.g 'r=0.001' corresponds to 20% of masked sky
            the lower 'r', the bigger is the portion of the sky is blinded. 
    Ouputs: - the boolean mask, derived from the HFI Planck sky map
            - the portion of the masked sky
    '''
    hf_m = maps[np.argmax(freq)]   #taking the map with the highest frequency inside the 'maps' array
    T_gal = r * np.amax(hf_m)    #define 'T_gal' which is an arbitrary temperature value above which we select the 'brightest' pixels from the galactic disk
    mask = np.copy(hf_m)*0 + 1
    mask[hf_m > T_gal] = 0
    ratio = len(np.where(mask == 0)[0]) / len(hf_m) * 100  #compute the ratio between the masked pixels (null value) and the total number of pixels
    masked_map = hf_m *mask
    #Define a boolean mask:
    size = len(masked_map)
    bool_mask  = np.zeros(size, dtype=bool)
    bool_mask[masked_map == 0] = True
    #new_masked_map = ma.masked_array(masked_map, bool_mask)
    
    return mask, bool_mask, ratio,'% of masked sky'

#hp.write_map(main_path+"Masks/mask_20_purcent", GetMask(0.001)[0])
#hp.write_map(main_path+"Masks/bool_mask_20_purcent", GetMask(0.001)[1])

mask_20 = hp.read_map(main_path+"Masks/mask_20_purcent")    
bool_mask_20 = hp.read_map(main_path+"Masks/bool_mask_20_purcent")    

def Mask(m, mask, bool_mask):
    '''
    Inputs:  - matrix 'm' (array like) the map we want to mask,
    
             - 'mask' (array like) that will allow to mask the map 'm'
             - 'bool_mask' (array like) the associate boolean mask that will allow to remove the null values in the map 'm' array
               
    Output: - the masked map array where the null values from the mask are no longer taken into account
             
    '''
    masked_map = m * mask
    new_masked_map = ma.masked_array(masked_map, bool_mask)
    
    return new_masked_map.data


##Get the masked maps:
    '''
    INPUTS: Smooth_degraded_converted maps
            Apply Mask to blind the Galatic disk signals
    
    OUTPUTS: Masked_maps files
    '''
#We choose here r= 0.001, that covers roughly 21% of the sky.
#for i in range(len(freq)):
 #   hp.write_map("/home/bajou/p1_bajou_hoa/ComputerProject/plots/masked_maps/masked_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(Mask(maps[i], 0.001)[1])+"_%_masked", Mask(maps[i], 0.001)[0])


#Create new masked maps by removing the null values from the masked pixels (Galactic disk) inside the masked maps:





masked_full   = [] 
masked_half_1 = []
masked_half_2 = []


for i in range(len(freq)):   
    masked_full.append(hp.read_map(main_path+"new_masked_full_data/masked_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_20_purcent_masked"))
    masked_half_1.append(hp.read_map(main_path+"masked_half_mission_1/masked_half_1_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_20_purcent_masked"))
    masked_half_2.append(hp.read_map(main_path+"masked_half_mission_2/masked_half_2_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_20_purcent_masked"))


#Extract the CMB skymap from the new masked_maps applying the ILC method :

'''
f_c     = np.ones(len(freq))   #for the CMB emission (ILC at 1 component)
icov    = Inv_cov_mat(masked_full)  #inverse of the covariance matrix
weights = Weights(f_c, icov)  #weights of the Planck maps
CMB     = Final_map(weights, masked_full)   #final CMB map obtained from the 1-component ILC method 
'''
#hp.mollview(CMB, norm = 'hist', unit="K_cmb", title= 'CMB skymap with mask covering '+str(Mask(maps[0], 0.001)[1])+'% of the sky,  nside = '+str(NSIDE)+" pixels.")


##For the constrained ILC:

F = np.array([np.ones(len(freq)),np.array([-0.24815, -0.35923, 5.152, 0.161098, 0.06918, 0.0380])])   #Matrix of constraints: the first line is for the CMB components, the second array contains the coefficients for the SZ component 
e = np.array([1,0])  #select the CMB, and reject the SZ effect

#Weights: 
#W_c = Weights_const(e.T, F.T, icov)    #'e' should be a column vector of length equal to the number of astrophysical components (here two: CMB and SZ), and 'F' a (6x2) matrix in this case, when taking into account 2 constraints

#CMB_c = Final_map(W_c, new_masked_maps)   #CMB map with the constrained ILC

#hp.mollview(CMB_c, norm = 'hist', unit="K_cmb", title= 'CMB skymap, nside = 2048 pixels') #with mask covering '+str(Mask(maps[0], 0.001)[1])+'% of the sky,  nside = '+str(NSIDE)+" pixels.")









