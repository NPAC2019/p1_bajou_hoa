#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#Compute the power spectrum of the CMB map

"""
Created on Wed Mar 13 16:37:41 2019

@author: bajou
"""

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt


NSIDE = 2048  
mask = 0.20    #portion of the skymap that is masked

main_path = "/home/bajou/p1_bajou_hoa/ComputerProject/plots/"
#mask_20 = hp.read_map(main_path+"Masks/mask_20_purcent")  

#plt.ion()
CMB     = hp.read_map(main_path+"CMB_map_nside_2048_20%_masked")
CMB_c   = hp.read_map(main_path+"constrained_CMB_map_nside_2048_20%_masked")
CMB_ref = hp.read_map("/home/abeelen/Planck/maps/COM_CMB_IQU-smica-field-Int_2048_R2.00.fits")
 
def Power_spec(m, l):
    '''
    Return the C_l coefficients from the decomposition of the map 'm' on the spherical harmonic basis
    '''
    return hp.anafast(m, lmax = l)

Lmax=1800  #define the maximum multipole value 

C_l = Power_spec(CMB, Lmax)
C_l_const = Power_spec(CMB_c, Lmax)  #C_l for the constrained ILC
C_l_ref = Power_spec(CMB_ref, Lmax)  #C_l for the Planck data

L = np.arange(len(C_l_const))

f = 1 - mask   #Correction factor due to the portion of the masked sky
gauss_beam = hp.gauss_beam(9.66*np.pi/(60*180), Lmax)    #Correction factor due to the smoothing of the skymaps
pix = hp.pixwin(NSIDE)[0:(Lmax+1)]   #Correction factor due to the pixelisation of maps

fin_corr= 1 / (f * gauss_beam**2 * pix**2 )   #Final correction factor


def Final_spec(m, l, lmax, corr):
    '''
    Return the corrected C_l coefficients, for plotting the final power spectrum
    '''    
    return corr * l * (l+1) *Power_spec(m, lmax)/(2*np.pi) 


pw   =  Final_spec(CMB, L, Lmax, fin_corr)   #power spectrum obtained with simple ILC
pw_const = Final_spec(CMB_c, L, Lmax, fin_corr)   #power spectrum obtained with constrained ILC
pw_ref  = L * (L+1) *Power_spec(CMB_ref, Lmax)/(2*np.pi)   #power spectrum of Planck data
        

print("We plot the power spectrum for the simple ILC (pw), constrained ILC (pw_const), and the pure CMB from the Planck data.")
   
plt.figure()
plt.title("Power spectrum from CMB maps at nside = "+str(NSIDE)+" pixels", fontsize='17')
plt.xlabel("$Multipole\,\,l$", fontsize='15')
plt.ylabel("$C_ll(l+1)/2\pi\,\,[K_{CMB}^2]$", fontsize='15')
plt.grid()
plt.plot(L, pw,'r.', label = "CMB from simple ILC, 20% masked sky")
plt.plot(L, pw_const, 'g.', label = "CMB from constrained ILC, "+str(mask * 100)+"% masked sky")
plt.plot(L, pw_ref, 'b^', label = "pure CMB")

plt.legend(loc='best')

plt.show()
