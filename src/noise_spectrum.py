#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#Compute the power spectrum of the noise map
"""
Created on Thu Mar 14 17:42:24 2019

@author: bajou
"""

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt
from ILC import Inv_cov_mat, Weights_const, Final_map, masked_half_1, masked_half_2

NSIDE = 2048

plt.ion()

freq = np.array([100,143,217,353,545,857])
freq_dic = {100: 100, 143: 143, 217: 217, 353: 353, 545: 545, 857: 857}

CMB_ref = hp.read_map("/home/abeelen/Planck/maps/COM_CMB_IQU-smica-field-Int_2048_R2.00.fits")
main_path = "/home/bajou/p1_bajou_hoa/ComputerProject/plots/"
noise_maps = []

mask_half_mission_1 = masked_half_1
mask_half_mission_2 = masked_half_2

print("ok")
##Get the noise skymaps:

for i in range(len(freq)):
    half_diff = (hp.read_map(main_path+"masked_half_mission_2/masked_half_2_planck_map_"+str(freq[i])+"_GHz_nside_2048_20_purcent_masked") - hp.read_map(main_path+"masked_half_mission_1/masked_half_1_planck_map_"+str(freq[i])+"_GHz_nside_2048_20_purcent_masked")) / 2 
    noise_maps.append(half_diff)
   # mask_half_mission_1.append(hp.read_map(main_path+"masked_half_mission_1/masked_half_1_planck_map_"+str(freq[i])+"_GHz_nside_2048_20_purcent_masked"))
    #mask_half_mission_2.append(hp.read_map(main_path+"masked_half_mission_2/masked_half_2_planck_map_"+str(freq[i])+"_GHz_nside_2048_20_purcent_masked"))
 
##Derive the final noise map:    
    
f_c = np.ones(len(freq))
F = np.array([np.ones(len(freq)),np.array([-0.24815, -0.35923, 5.152, 0.161098, 0.06918, 0.0380])])
e = np.array([1,0])

icov_n = Inv_cov_mat(noise_maps)
icov_h1 = Inv_cov_mat(mask_half_mission_1)
icov_h2 = Inv_cov_mat(mask_half_mission_2)
print("ok")
weights_n = Weights_const(e.T, F.T, icov_n)
weights_h1 = Weights_const(e.T, F.T, icov_h1)
weights_h2 = Weights_const(e.T, F.T, icov_h2)
print("ok")
final_noise_map = Final_map(weights_n, noise_maps)
final_CMB_h1 = Final_map(weights_h1,mask_half_mission_1)
final_CMB_h2 = Final_map(weights_h2,mask_half_mission_2)
print("ok")
'''
icov_n = ilc.Inv_cov_mat(noise_maps)
icov_h1 = ilc.Inv_cov_mat(mask_half_mission_1)
icov_h2 = ilc.Inv_cov_mat(mask_half_mission_2)

weights_n = ilc.Weights_const(e.T, F.T, icov_n)
weights_h1 = ilc.Weights_const(e.T, F.T, icov_h1)
weights_h2 = ilc.Weights_const(e.T, F.T, icov_h2)

final_noise_map = ilc.Final_map(weights_n, noise_maps)
final_CMB_h1 = ilc.Final_map(weights_h1,mask_half_mission_1)
final_CMB_h2 = ilc.Final_map(weights_h2,mask_half_mission_2)
'''

#hp.write_map(main_path+"half_mission_1_CMB_map_nside_2048_20_purcent_masked", final_CMB_h1)
#hp.write_map(main_path+"half_mission_2_CMB_map_nside_2048_20_purcent_masked", final_CMB_h2)
#hp.write_map(main_path+"noise_map_nside_2048_20_purcent_masked", final_noise_map)


##Get the power spectrum:

Lmax=1800    #define the maximum multipole value , i.e the smallest angular scale

#Compute the C_l coefficients : 
C_l_noise = hp.anafast(final_noise_map, lmax = Lmax)   #C_l for the noise map
C_l_cross = hp.anafast(final_CMB_h1, final_CMB_h2, lmax = Lmax)  #C_l from the cross spectrum between half_missions maps


L = np.arange(Lmax+1)

#Apply the corrections due to mask, beaming, and pixelisation: 
corr = 0.8  #Correction factor due to the portion of the masked sky
gauss_beam = hp.gauss_beam(9.66*np.pi/(60*180), Lmax)    #Correction factor due to the smoothing of the skymaps
pix = hp.pixwin(NSIDE)[0:(Lmax+1)]   #Correction factor due to the pixelisation of maps

fin_corr= 1 / (corr * gauss_beam**2 * pix**2)

#Compute the final power spectrum:
pw_noise = fin_corr * L * (L+1) *C_l_noise /(2*np.pi)
pw_cross = fin_corr * L * (L+1) *C_l_cross /(2*np.pi)
pw_ref  = L * (L+1) * hp.anafast(CMB_ref, lmax = Lmax)/(2*np.pi) 


print("We plot the cross spectrum from the half-mission maps, compared to the CMB")
plt.figure()
plt.title("Power spectrum from CMB maps at nside = "+str(NSIDE)+" pixels", fontsize='17')
plt.xlabel("$Multipole\,\,l$", fontsize='15')
plt.ylabel("$C_ll(l+1)/2\pi\,\,[K_{CMB}^2]$", fontsize='15')
plt.grid()
plt.plot(L, pw_cross, 'r+', label = "cross spectrum between half-mission maps")
plt.plot(L, pw_ref, 'b^', label = "pure CMB")
plt.legend()

plt.show()


