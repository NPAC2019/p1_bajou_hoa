#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 11:42:14 2019

@author: bajou
"""

import healpy as hp
import numpy as np


freq = np.array([100,143,217,353,545,857])   #the frequencies of the Planck skymaps (GHz)
freq_dic = {100: 100, 143: 143, 217: 217, 353: 353, 545: 545, 857: 857}

main_path = "/home/bajou/p1_bajou_hoa/ComputerProject/plots/"




NSIDE = 2048

'''    

def which_maps(data, noise):
    
    if data:
        maps = []
        for i in range(len(freq)):
            maps.append(hp.read_map("/home/bajou/p1_bajou_hoa/ComputerProject/plots/smooth/smooth_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))
        return maps
        
    if noise:
        noise_maps = []
        for i in range(len(freq)):
            noise_diff = (hp.read_map(main_path+"half_mission_2/half_2_planck_map_"+str(freq[i])+"_GHz_nside_2048") - hp.read_map(main_path+"half_mission_1/half_1_planck_map_"+str(freq[i])+"_GHz_nside_2048")) / 2 
            noise_maps.append(noise_diff)
        return maps
        
'''        
    
maps = []
for i in range(len(freq)):
    maps.append(hp.read_map("/home/bajou/p1_bajou_hoa/ComputerProject/plots/smooth/smooth_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))
   


noise_maps = []
for i in range(len(freq)):
    noise_diff = (hp.read_map(main_path+"half_mission_2/half_2_planck_map_"+str(freq[i])+"_GHz_nside_2048") - hp.read_map(main_path+"half_mission_1/half_1_planck_map_"+str(freq[i])+"_GHz_nside_2048")) / 2 
    noise_maps.append(noise_diff)
    
