#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#Smooth and degrade the Planck maps
"""
Created on Mon Mar 11 14:46:44 2019

@author: bajou
"""

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt

plt.ion()

freq= np.array([100,143,217,353,545,857])
freq_dic = {100: 100, 143: 143, 217: 217, 353: 353, 545: 545, 857: 857}

FWHM= np.array([9.66, 7.27,5.01, 4.86, 4.84, 4.63])
FWHM_dic = {100: 9.66, 143: 7.27, 217: 5.01, 353: 4.86, 545: 4.84, 857: 4.63}

conv_fac= {100: 1, 143: 1, 217: 1, 353: 1, 545: 1/58.04, 857: 1/2.27}   #correction factors from MJy/sr to K_cmb

NSIDE=2048

path_full = "full"
path_half_1 = "halfmission-1"
path_half_2 = "halfmission-2"

def smooth_and_degrade(f, nside, path):
    '''
    Inputs: - 'f' (int type) the frequency of a Planck map; 
            - 'nside' (int type, multiple of two of the total NSIDE number of pixels);
            - 'path' (str type) the name of the map one wants to smooth, degrade, and convert to Kcmb; either the full map or the half-mission ones.
    Output: 'conv_map' (array like) the converted, degraded, and smoothed map
    '''
    if f in freq:
        
        filename = "/home/abeelen/Planck/maps/HFI_SkyMap_" + str(f) + "_2048_R2.00_"+path+".fits"
        original_map = hp.read_map(filename)
        new_FWHM = np.sqrt(np.max(FWHM)**2-FWHM_dic[f]**2)*np.pi/(60*180)  #the FWHM is converted from arcmins to radians 
        smooth_map = hp.smoothing(original_map, new_FWHM)     #smoothed map with a gaussian distribution whose parameter is 'new_FWHM'
        deg_map = hp.ud_grade(smooth_map, nside)   #degrade the map to nside pixels
      
        conv_map = deg_map * conv_fac[f]    #conversion into K_cmb
        
        return conv_map #, norm='hist', unit="K_cmb", title= 'Planck map at f='+str(f) +' GHz, nside = '+str(nside))
    
    else: 
        
        return "f should be one of the frequencies of the Planck maps."


#for i in range(len(freq)):
 #   hp.write_map("/home/bajou/p1_bajou_hoa/ComputerProject/plots/smooth/smooth_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE), smooth_and_degrade(freq[i], NSIDE))


