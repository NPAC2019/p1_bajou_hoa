# -*- coding: utf-8 -*-
#load and reading the data from the Planck maps
#BAJOU Raphaël

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt

plt.ion()

frequency= np.array([100,143,217,353,545,857])

filename= "/home/bajou/p1_bajou_hoa/ComputerProject/plots/smooth_degrade/smooth_planck_map_100_GHz_nside_1024"


m= hp.read_map(filename)
#print(np.shape(m))
hp.mollview(m, norm= "hist")
    

