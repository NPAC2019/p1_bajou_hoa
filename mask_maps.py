#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 17:14:26 2019

@author: hoa
"""

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import inv
import numpy.ma as ma

plt.ion()

NSIDE = 2048 #pixel resolution of the skymaps

freq= np.array([100,143,217,353,545,857])
freq_dic = {100: 100, 143: 143, 217: 217, 353: 353, 545: 545, 857: 857}

FWHM= np.array([9.66, 7.27,5.01, 4.86, 4.84, 4.63])
FWHM_dic = {100: 9.66, 143: 7.27, 217: 5.01, 353: 4.86, 545: 4.84, 857: 4.63}

maps = []
half_mission_1_maps = []
half_mission_2_maps = []

for i in range(len(freq)):
    maps.append(hp.read_map("/home/hoa/ComputerProject/plots/smooth_degrade/smooth_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))

#for i in range(len(freq)):
#    masked_maps.append(hp.read_map("/home/hoa/ComputerProject/plots/masked_maps/masked_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(blind)+"_%_masked"))

for i in range(len(freq)):
    half_mission_1_maps.append(hp.read_map("/home/hoa/ComputerProject/plots/smooth_degrade/smooth_planck_map_half_mission_1_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))    

for i in range(len(freq)):
    half_mission_2_maps.append(hp.read_map("/home/hoa/ComputerProject/plots/smooth_degrade/smooth_planck_map_half_mission_2_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))    

def Cov_mat(m):
    cov=np.zeros((len(freq),len(freq)))
    
    for i in range(len(freq)):
        meani  = np.mean(m[i])      
        npix   = np.size(m[i])      
        for j in range(len(freq)):
            meanj  = np.mean(m[j])
            cov[i][j] = (m[i]-meani).dot(m[j]-meanj) / npix
    
    return inv(cov)


def Weights(f_c, icov):
    '''
    Inputs: - 'f_c' (array like)
            - 'i_cov' (array like): inverse of the covariance matrix
    Output: - the weights obtained with the ILC method
    '''
    return f_c.T.dot(icov)/(f_c.T.dot(icov).dot(f_c))

def Weights_const(e, F, icov):
    
    return e.T.dot(inv(F.T.dot(icov).dot(F))).dot(F.T).dot(icov)

    
def Final_map(w,m):
    '''
    Inputs: 'w' (array like) whose elements are the weights of each Planck map, 'm' (array like) whose elements are the skymaps;
    Output: the final linear combination of the weighted maps (array like)
    '''
    return w.dot(m)


def Mask(m, r):
    '''
    Inputs:  - matrix 'm' (array like) the map on which we want to apply the mask,
             - 'r' (float type) which defines the criteria to mask the sky,
               the lower 'r', the bigger is the portion of the sky is blinded. 
    Outputs: - the masked map
             - the portion of the sky that is masked.
    '''
    hf_m = maps[np.argmax(freq)]   #taking the map with the highest frequency inside the 'maps' array
    T_gal = r * np.amax(hf_m)    
    mask = np.copy(hf_m)*0 + 1
    mask[hf_m > T_gal] = 0
    ratio = len(np.where(mask == 0)[0]) / len(hf_m) * 100    #compute the ratio between the masked pixels (null value) and the total number of pixels
    
    return m * mask, int(ratio),"% of the sky is masked."

##############################################################################
#Get the masked maps:
    '''
    INPUTS: Smooth_degraded_converted maps
            Apply Mask to blind the Galatic disk signals
    
    OUTPUTS: Masked_maps files
    '''
blind = 0.001
for i in range(len(freq)):
    hp.write_map("/home/hoa/ComputerProject/plots/masked_maps/masked_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(Mask(maps[i], blind)[1])+"_%_masked", Mask(maps[i], blind)[0])

for i in range(len(freq)):
    hp.write_map("/home/hoa/ComputerProject/plots/masked_maps/masked_planck_map_half_mission_1_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(Mask(maps[i], blind)[1])+"_%_masked", Mask(half_mission_1_maps[i], blind)[0])

for i in range(len(freq)):
    hp.write_map("/home/hoa/ComputerProject/plots/masked_maps/masked_planck_map_half_mission_2_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(Mask(maps[i], blind)[1])+"_%_masked", Mask(half_mission_2_maps[i], blind)[0])
