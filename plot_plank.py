#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 18:40:00 2019

@author: hoa
"""

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import inv
import numpy.ma as ma

plt.ion()

NSIDE = 2048 #pixel resolution of the skymaps

freq= np.array([100,143,217,353,545,857])
freq_dic = {100: 100, 143: 143, 217: 217, 353: 353, 545: 545, 857: 857}

FWHM= np.array([9.66, 7.27,5.01, 4.86, 4.84, 4.63])
FWHM_dic = {100: 9.66, 143: 7.27, 217: 5.01, 353: 4.86, 545: 4.84, 857: 4.63}


maps = []
masked_maps = []


for i in range(len(freq)):
    masked_maps.append(hp.read_map("/home/hoa/ComputerProject/plots/masked_maps/masked_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(20)+"_%_masked"))


for i in range(len(freq)):
    maps.append(hp.read_map("/home/hoa/ComputerProject/plots/smooth_degrade/smooth_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))
    
hp.mollview(maps[0], norm = 'hist', unit="K_cmb", title= 'CMB skymap at '+str(freq[0])+'GHz, nside = '+str(NSIDE)+' pixels.') #without mask covering '+str(20)+'% of the sky,  nside = '+str(NSIDE)+" pixels.")
hp.mollview(masked_maps[0], norm = 'hist', unit="K_cmb", title= 'CMB skymap masked 20% at '+str(freq[0])+'GHz, nside = '+str(NSIDE)+' pixels.')

