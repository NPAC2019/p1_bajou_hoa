#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 18:21:27 2019

@author: hoa
"""

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import inv
import numpy.ma as ma

plt.ion()

NSIDE = 2048 #pixel resolution of the skymaps
BLIND_sky = 20

freq= np.array([100,143,217,353,545,857])
freq_dic = {100: 100, 143: 143, 217: 217, 353: 353, 545: 545, 857: 857}

FWHM= np.array([9.66, 7.27,5.01, 4.86, 4.84, 4.63])
FWHM_dic = {100: 9.66, 143: 7.27, 217: 5.01, 353: 4.86, 545: 4.84, 857: 4.63}

maps = []
for i in range(len(freq)):
    maps.append(hp.read_map("/home/hoa/ComputerProject/plots/smooth_degrade/smooth_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)))

masked_maps = []
masked_hfm_1 = []
masked_hfm_2 = []

for i in range(len(freq)):
    masked_maps.append(hp.read_map("/home/hoa/ComputerProject/plots/masked_maps/masked_planck_map_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(BLIND_sky)+"_%_masked"))

for i in range(len(freq)):
    masked_hfm_1.append(hp.read_map("/home/hoa/ComputerProject/plots/masked_maps/masked_planck_map_half_mission_1_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(BLIND_sky)+"_%_masked"))

for i in range(len(freq)):
    masked_hfm_2.append(hp.read_map("/home/hoa/ComputerProject/plots/masked_maps/masked_planck_map_half_mission_2_"+str(freq[i])+"_GHz_nside_"+str(NSIDE)+"_"+str(BLIND_sky)+"_%_masked"))
  
###########################################################
def Cov_mat(m):
    cov=np.zeros((len(freq),len(freq)))
    
    for i in range(len(freq)):
        meani  = np.mean(m[i])      
        npix   = np.size(m[i])      
        for j in range(len(freq)):
            meanj  = np.mean(m[j])
            cov[i][j] = (m[i]-meani).dot(m[j]-meanj) / npix
    
    return inv(cov)


def Weights(f_c, icov):
    '''
    Inputs: - 'f_c' (array like)
            - 'i_cov' (array like): inverse of the covariance matrix
    Output: - the weights obtained with the ILC method
    '''
    return f_c.T.dot(icov)/(f_c.T.dot(icov).dot(f_c))

def Weights_const(e, F, icov):
    
    return e.T.dot(inv(F.T.dot(icov).dot(F))).dot(F.T).dot(icov)

    
def Final_map(w,m):
    '''
    Inputs: 'w' (array like) whose elements are the weights of each Planck map, 'm' (array like) whose elements are the skymaps;
    Output: the final linear combination of the weighted maps (array like)
    '''
    return w.dot(m)

##########################################################
#Create new masked maps by removing the null values from the masked pixels (Galactic disk) inside the masked maps:    
new_masked_maps = [] 

#Define a boolean mask:
size = len(masked_maps[0])
bool_mask  = np.zeros(size, dtype=bool)
bool_mask[masked_maps == 0] = True

for i in range(len(freq)):   
    masked_map = masked_maps[i]    
    new_masked_maps.append(ma.masked_array(masked_map, bool_mask))
    del(masked_map)

########################################################################
#Extract the CMB skymap from the new masked_maps by ILC unconstrained: 
f_c     = np.ones(len(freq))   #for the CMB emission 
icov    = Cov_mat(new_masked_maps)  #inverse of the covariance matrix
icov_unmasked = Cov_mat(maps)

weights = Weights(f_c, icov)  #weights of the Planck maps
weights_unmasked = Weights(f_c, icov_unmasked)

CMB_free     = Final_map(weights, new_masked_maps)
CMB_free_unmasked = Final_map(weights_unmasked,maps)

#WRITE CMB MAPS_Unconstrained
#hp.write_map("/home/hoa/ComputerProject/plots/CMB_maps/CMB_skymap_free_"+str(NSIDE)+"_"+str(BLIND_sky)+"_%_masked",CMB_free)
hp.mollview(CMB_free, norm = 'hist', unit="K_cmb", title= 'CMB skymap from ILC_simple with mask covering '+str(BLIND_sky)+'% of the sky,  nside = '+str(NSIDE)+" pixels.")

hp.write_map("/home/hoa/ComputerProject/plots/CMB_maps/CMB_skymap_free_"+str(NSIDE)+"_unmasked",CMB_free_unmasked)
hp.mollview(CMB_free_unmasked, norm = 'hist', unit="K_cmb", title= 'CMB skymap from ILC_simple, nside = '+str(NSIDE)+" pixels.")

########################################################################
#Extract CMB_map by ILC_Constrained 
#Only select CMB, reject SZ effects
F_obs = np.array([np.ones(len(freq)),np.array([-0.24815, -0.35923, 5.152, 0.161098, 0.06918, 0.0380])])
e_selector = np.array([1,0])    #Select CMB and reject SZ effects

weight_constrains = Weights_const(e_selector.T, F_obs.T, icov)

weight_constrains_unmasked = Weights_const(e_selector.T, F_obs.T, icov_unmasked)
CMB_constrained_unmasked = Final_map(weight_constrains_unmasked, maps)
hp.write_map("/home/hoa/ComputerProject/plots/CMB_maps/CMB_skymap_constrained_"+str(NSIDE)+"_unmasked",CMB_constrained_unmasked)
hp.mollview(CMB_constrained_unmasked, norm = 'hist', unit="K_cmb", title= 'CMB skymap from ILC_constrained, nside = '+str(NSIDE)+" pixels.")



CMB_constrained = Final_map(weight_constrains, new_masked_maps)   
#hp.write_map("/home/hoa/ComputerProject/plots/CMB_maps/CMB_skymap_constrained_"+str(NSIDE)+"_"+str(BLIND_sky)+"_%_masked",CMB_constrained)
hp.mollview(CMB_constrained, norm = 'hist', unit="K_cmb", title= 'CMB skymap from ILC_constrained with mask covering '+str(BLIND_sky)+'% of the sky,  nside = '+str(NSIDE)+" pixels.")


#################################################
new_masked_half_miss_1_maps = []
new_masked_half_miss_2_maps = []  

#Define a boolean mask:
half_1_size = len(masked_hfm_1[0])
half_1_bool_mask  = np.zeros(half_1_size, dtype=bool)
half_1_bool_mask[masked_hfm_1[0] == 0] = True

for i in range(len(freq)):   
    masked_map = masked_hfm_1[i]
    new_masked_half_miss_1_maps.append(ma.masked_array(masked_map, half_1_bool_mask))
    del(masked_map)
    
#Define a boolean mask:
half_2_size = len(masked_hfm_2[0])
half_2_bool_mask  = np.zeros(half_2_size, dtype=bool)
half_2_bool_mask[masked_hfm_2[0] == 0] = True

for i in range(len(freq)):   
    masked_map = masked_hfm_2[2]
    new_masked_half_miss_2_maps.append(ma.masked_array(masked_map, half_2_bool_mask))
    del(masked_map)    

noise_masked_maps = (np.array(new_masked_half_miss_1_maps) - np.array(new_masked_half_miss_2_maps))/2
    
hf_c = np.ones(len(freq))   #for the CMB emission 
icov_noise = Cov_mat(noise_masked_maps)  #inverse of the covariance matrix

weights_noise = Weights(hf_c, icov_noise)  #weights of the Planck maps
Noise_map = Final_map(weights_noise, noise_masked_maps)    

#hp.write_map("/home/hoa/ComputerProject/plots/Noise_maps/Noise_map_"+str(NSIDE)+"_"+str(BLIND_sky)+"_%_masked",Noise_map)

#################################################
#Random Noise
sigma_noise = np.amax(Noise_map)
Random_noise = np.ma.asarray(np.random.normal(0, sigma_noise, Noise_map.shape))


#hp.write_map("/home/hoa/ComputerProject/plots/Noise_maps/Random_Noise, Random_noise)

#################################################
CMB_ref = hp.read_map("/home/abeelen/Planck/maps/COM_CMB_IQU-smica-field-Int_2048_R2.00.fits")

def Power_spec(cmb, l):
    return hp.anafast(cmb, lmax = l)

lmax = 2150
Cl_ref = Power_spec(CMB_ref, lmax)
Cl_free = Power_spec(CMB_free, lmax)
Cl_constrains = Power_spec(CMB_constrained, lmax)

Cl_noise = Power_spec(Noise_map, lmax)
Cl_random_noise = Power_spec(Random_noise, lmax)

l = np.arange(len(Cl_ref))

#Numerical Correction to recover a true CMB_sky map
fraction_sky = 1/(1-(BLIND_sky/100))

#Correction from Gaussian smoothing function
fwhm = 9.66 * (np.pi)/(60*180)
gauss_beam_correction = hp.gauss_beam(fwhm, lmax)
pix = hp.pixwin(NSIDE)[0:(lmax+1)]   #Correction factor due to the pixelisation of maps

correction_factor = fraction_sky * (1/gauss_beam_correction**2) * (1/pix**2)

#True CMB_sky_map
pw_free = correction_factor * (l * (l+1)) * Cl_free/(2*np.pi) #With fraction of the sky
pw_constrains = correction_factor * (l * (l+1)) * Cl_constrains/(2*np.pi)
pw_CMB_pure = l * (l+1) * Cl_ref/(2*np.pi)

pw_Noise = correction_factor * (l*(l+1)) * Cl_noise/(2*np.pi)
pw_Random = correction_factor * (l*(l+1)) * Cl_random_noise/(2*np.pi)*np.e**(-8)

cutting_rescale = []
for i in range(1500,lmax):
    cutting_rescale.append(pw_constrains[i])

cutting_noise = []
for i in range(1500,lmax):
    cutting_noise.append(pw_Random[i])    
scalling = np.mean(cutting_noise) / (np.mean(cutting_rescale))


pw_CMB_Noise = correction_factor * (l*(l+1)) * (Cl_constrains - Cl_noise )/(2*np.pi)
pw_Random_rescaled = pw_Random/(1.96*scalling)
pw_CMB_recovered = correction_factor * (l*(l+1)) * (Cl_constrains - Cl_noise )/(2*np.pi) - pw_Random_rescaled

#Plot power spectrum
plt.figure()
plt.title("Power spectrums from CMB maps at nside = "+str(NSIDE)+" pixels", fontsize='17')
plt.xlabel("$Multipole\,\,l$", fontsize='15')
plt.ylabel("$C_ll(l+1)/2\pi\,\,[K_{CMB}^2]$", fontsize='15')


#plt.plot(l, pw_free,'r.', label = "CMB from simple ILC, "+str(BLIND_sky)+"% masked sky")
plt.plot(l, pw_constrains,'g.', label = "CMB spectrum from constrained ILC with noise,"+str(BLIND_sky)+"% masked sky")
plt.plot(l, pw_CMB_pure, 'b+', label = "Planck CMB spectrum")
plt.plot(l, pw_Noise, 'r.', label = "Noise")
plt.plot(l, pw_Random_rescaled, 'c+', label = "Random_noise")
plt.plot(l, pw_CMB_Noise, 'm^' ,label = "CMB spectrum - Noise")
plt.plot(l, pw_CMB_recovered, 'y.' ,label = "CMB spectrum - Noise - Random_noise")
plt.grid()
plt.legend(loc='best')
plt.show()



